package id.web.knt.po.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="purchase_order")
@NamedQueries({
	@NamedQuery(name="PurchaseOrder.findAll", query="SELECT po FROM PurchaseOrder po"),
	@NamedQuery(name="PurchaseOrder.findByContact", query="SELECT po FROM PurchaseOrder po WHERE po.contact = :contact")
})
public class PurchaseOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	@Column(name="transaction_date")
	@Temporal(TemporalType.DATE)
	private Date date = new Date();
	@Column(name="transaction_number")
	private String transactionNumber;
	@ManyToOne
	@JoinColumn(name="user_id")
	private Employee user;
	@Column(name="contact")
	private String contact;
	@Transient
	private BigDecimal total = BigDecimal.ZERO;
	
	@OneToMany(mappedBy="purchaseOrder", fetch=FetchType.EAGER)
	private List<PurchaseOrderDetail> details = new ArrayList<PurchaseOrderDetail>();

	@Override
	public String toString() {
		return "PurchaseOrder [id=" + id + ", date=" + date
				+ ", transactionNumber=" + transactionNumber + ", user=" + user
				+ ", contact=" + contact + ", details=" + details + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((transactionNumber == null) ? 0 : transactionNumber
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrder other = (PurchaseOrder) obj;
		if (transactionNumber == null) {
			if (other.transactionNumber != null)
				return false;
		} else if (!transactionNumber.equals(other.transactionNumber))
			return false;
		return true;
	}
	
	public void calculateTotal() {
		total = BigDecimal.ZERO;
		for (PurchaseOrderDetail detail : details) {
			total= total.add(detail.getTotal());
			System.out.println("sub total : " + detail.getTotal() +" Jadinya : "+total);
		}
	}
	
	public void addDetail(PurchaseOrderDetail purchaseOrderDetail) {
		if (purchaseOrderDetail != null) {
			purchaseOrderDetail.setPurchaseOrder(this);
			details.add(purchaseOrderDetail);
		}
	}
	
	public void removeDetail(PurchaseOrderDetail purchaseOrderDetail) {
		if (purchaseOrderDetail != null) {
			details.remove(purchaseOrderDetail);
			purchaseOrderDetail.setPurchaseOrder(null);
		}
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Employee getUser() {
		return user;
	}

	public void setUser(Employee user) {
		this.user = user;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Long getId() {
		return id;
	}

	public List<PurchaseOrderDetail> getDetails() {
		return details;
	}

	public BigDecimal getTotal() {
		return total;
	}
	
}
