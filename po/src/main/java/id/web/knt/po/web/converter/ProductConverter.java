package id.web.knt.po.web.converter;

import id.web.knt.po.entity.Product;

import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("productConverter")
@Stateless
public class ProductConverter implements Converter {
	@PersistenceContext
	private EntityManager em;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 == null || arg2.equals(""))
			return null;
	    Long id = Long.valueOf(arg2);
	    return em.find(Product.class, id);
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (!(arg2 instanceof Product))
			return "";
	    Product product = (Product) arg2;
		return product.getId().toString();
	}

}
