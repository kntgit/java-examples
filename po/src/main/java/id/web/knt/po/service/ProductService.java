package id.web.knt.po.service;

import id.web.knt.po.entity.Product;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ProductService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;
	
	public List<Product> findAllProducts() {
		return em.createQuery("select p from Product p", Product.class)
			.getResultList();
	}
	
	public void save(Product p) {
		em.merge(p);
	}
	
	public void delete(Product p) {
		Product px = em.find(Product.class, p.getId());
		em.remove(px);
	}
}
