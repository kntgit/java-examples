package id.co.knt.airport.web;

import id.co.knt.airport.entity.Fare;
import id.co.knt.airport.entity.FlightSchedule;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("fareBean")
@Stateful
@ViewScoped
public class FareBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	private static Logger log = LoggerFactory.getLogger(FareBean.class);

	private FlightSchedule selectedFlightSchedule;
	private Fare selectedFare;

	@Temporal(TemporalType.DATE)
	private Date todayDate = new Date();
	@Temporal(TemporalType.DATE)
	private Date fromDate = new Date();
	@Temporal(TemporalType.DATE)
	private Date toDate = new Date();

	@PostConstruct
	public void init() {
		reset();
	}

	public void reset() {
		selectedFare = new Fare();
	}

	public void create() {
		reset();
	}

	public String filterSchedule() {
		log.info("filterSchedule called 1");
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);
		String today = sdf.format(todayDate);

		if (fromDate != null && toDate != null) {
			if (from.equals(today)) {
				if (toDate.before(todayDate))
					expiredArrival();
			} else if (fromDate.after(todayDate)) {
				if (to.equals(today))
					arrivalMessage();
				else if (toDate.before(todayDate))
					expiredArrival();
			} else if (fromDate.before(todayDate)) {
				expiredDeparture();
				if (to.equals(today))
					log.info("to == today");
				else if (toDate.before(todayDate))
					expiredArrival();
			}
			log.info("filterSchedule called 2");
		}
		return null;
	}

	private String arrivalMessage() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"your arrival date can not before departure date"));
		log.info("arrival and departure can not same");

		return null;
	}

	private String expiredDeparture() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"your departure date is expired"));
		log.info("expiredDeparture called");

		return null;
	}

	private String expiredArrival() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"your arrival date is expired"));
		log.info("expiredArrival called");

		return null;
	}

	public String persist() {
		if (selectedFare.getId() == null)
			selectedFlightSchedule.addFare(selectedFare);
		em.merge(selectedFlightSchedule);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));

		return null;
	}

	public void onClose(CloseEvent e) {
	}

	public String delete() {
		System.out.println("fare di delete : " + selectedFare);
		System.out.println("yang mau di delete : " + selectedFare.getFareType());
		selectedFlightSchedule.removeFare(selectedFare);
		em.merge(selectedFlightSchedule);
		return null;
	}

	public Fare getSelectedFare() {
		return selectedFare;
	}

	public void setSelectedFare(Fare newFare) {
		this.selectedFare = newFare;
	}

	public FlightSchedule getSelectedFlightSchedule() {
		return selectedFlightSchedule;
	}

	public void setSelectedFlightSchedule(FlightSchedule selectedFlightSchedule) {
		this.selectedFlightSchedule = em.merge(selectedFlightSchedule);
		System.out.println("selectedFlightSchedule = " + selectedFlightSchedule);
	}

	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	@Remove
	public void destroy() {
	}

}
