package id.co.knt.airport.web;

import id.co.knt.airport.entity.FlightSchedule;
import id.co.knt.airport.entity.Leg;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("legBean")
@Stateful
@ViewScoped
public class LegBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private static Logger log = LoggerFactory.getLogger(LegBean.class);
	
	private FlightSchedule selectedFlightSchedule;
	private Leg newLeg;
	
	@Temporal(TemporalType.DATE)
	private Date todayDate = new Date();
	@Temporal(TemporalType.DATE)
	private Date fromDate = new Date();
	@Temporal(TemporalType.DATE)
	private Date toDate = new Date();
	
	@PostConstruct
	public void init(){
		reset();
	}
	
	public void reset(){
		newLeg = new Leg();
	}
	
	public void create(){
		reset();
	}
		
	public String filterSchedule(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);
		String today = sdf.format(todayDate);
	
		if(fromDate != null && toDate != null){
			if(from.equals(today)){
				if(toDate.before(todayDate))
					expiredArrival();		
			}
			else if(fromDate.after(todayDate)){
				if(to.equals(today))
					arrivalMessage();
				else if(toDate.before(todayDate))
					expiredArrival();
			}			
			else if(fromDate.before(todayDate)){
				expiredDeparture();
				if(to.equals(today))
					log.info("to == today");
				else if(toDate.before(todayDate))
					expiredArrival();
			}
			log.info("filterSchedule called");
		}
		return null;
	}
			
	private String arrivalMessage(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your arrival date can not before departure date"));
		log.info("arrival and departure can not same");
		
		return null;
	}
	
	private String expiredDeparture(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your departure date is expired"));
		log.info("expiredDeparture called");
		
		return null;
	}
	
	private String expiredArrival(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("your arrival date is expired"));
		log.info("expiredArrival called");
		
		return null;
	}
	
	public String persist(){
		selectedFlightSchedule.addLeg(newLeg);
		em.merge(newLeg);
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
				
		return null;
	}
	
	
	public void onClose(CloseEvent e){
	}
	
	public Leg getNewLeg() {
		return newLeg;
	}
	
	public void setNewLeg(Leg newLeg) {
		this.newLeg = em.merge(newLeg);
	}
	
	public FlightSchedule getSelectedFlightSchedule() {
		return selectedFlightSchedule;
	}

	public void setSelectedFlightSchedule(FlightSchedule selectedFlightSchedule) {
		this.selectedFlightSchedule = em.merge(selectedFlightSchedule);
	}

	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}
	
	@Remove
	public void destroy(){}
	
	
}
