package id.co.knt.airport.web.converter;

import id.co.knt.airport.entity.FareType;

import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("fareTypeConverter")
@Stateless
public class FareTypeConverter implements Converter{
	@PersistenceContext
	EntityManager em;
	
	private static final Logger log = LoggerFactory.getLogger(FareTypeConverter.class);

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		log.info("getAsObject arg2 : "+ arg2);
		if (arg2 == null || arg2.equals(""))
			return null;
	    Long id = Long.valueOf(arg2);
	    FareType fareType = em.find(FareType.class, id);
	    return fareType;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		log.info("getAsString arg2 : "+ arg2);
		if (!(arg2 instanceof FareType))
			return "";
	    FareType fareType = (FareType) arg2;
	    Long id = fareType.getId();
	    return String.valueOf(id);
	}

}
