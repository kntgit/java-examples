package id.co.knt.airport.web.util;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class CloseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6904313700229968317L;

	public CloseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CloseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CloseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CloseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
