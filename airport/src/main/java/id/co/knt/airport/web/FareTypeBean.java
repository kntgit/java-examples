package id.co.knt.airport.web;

import id.co.knt.airport.entity.FareType;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

@Named("fareTypeBean")
@Stateful
@SessionScoped
public class FareTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	private FareType selectedFareType;
	
	@PostConstruct
	public void init() {
		reset();
	}
	
	public void reset() {
		selectedFareType = new FareType();
	}
	
	public String delete() {
		em.remove(selectedFareType);
		return null;
	}
	
	public void create() {
		reset();
	}
	
	public String persist() {
		em.merge(selectedFareType);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("saved"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("fareTypeDialog.hide()");
		
		return null;
	}
	
	public void onClose(CloseEvent e){
	}

	public FareType getSelectedFareType() {
		return selectedFareType;
	}

	public void setSelectedFareType(FareType selectedFareType) {
		this.selectedFareType = em.merge(selectedFareType);
	}
}
