package id.co.knt.airport.entity;

import java.io.Serializable;

public class FlightFare implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String model;
	private FlightSchedule flightSchedule;
	private Fare fare;
	
	
	public FlightFare(String model,FlightSchedule flightSchedule, Fare fare) {
		super();
		this.model=model;
		this.flightSchedule = flightSchedule;
		this.fare = fare;
	}
	public FlightSchedule getFlightSchedule() {
		return flightSchedule;
	}
	public void setFlightSchedule(FlightSchedule flightSchedule) {
		this.flightSchedule = flightSchedule;
	}
	public Fare getFare() {
		return fare;
	}
	public void setFare(Fare fare) {
		this.fare = fare;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	

}
