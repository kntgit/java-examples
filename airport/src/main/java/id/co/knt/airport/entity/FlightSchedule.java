package id.co.knt.airport.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "FLIGHT_SCHEDULES")
@NamedQueries({
		@NamedQuery(name = "FlightSchedule.findAll", query = "select fs from FlightSchedule fs"),
		@NamedQuery(name = "FlightSchedule.findByDeparture", query = "select fs from FlightSchedule fs where fs.departureAirport=:departure"),
		@NamedQuery(name = "FlightSchedule.findByArrival", query = "select fs from FlightSchedule fs where fs.arrivalAirport=:arrival"),
		@NamedQuery(name = "FlightSchedule.findByDepartureAndArrival", query = "select fs from FlightSchedule fs where fs.departureAirport=:departure and fs.arrivalAirport=:arrival"),
		@NamedQuery(name = "FlightSchedule.findByBetweenDate", query = "select fs from FlightSchedule fs where fs.departureDate between :fromDate and :toDate") })
public class FlightSchedule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	@Column(name = "FLIGHT_NUMBER")
	private String flightNumber;
	@ManyToOne
	@JoinColumn(name = "DEPARTURE_AIRPORT_ID")
	private Airport departureAirport;
	@ManyToOne
	@JoinColumn(name = "ARRIVAL_AIRPORT_ID")
	private Airport arrivalAirport;
	@Column(name = "DEPARTURE_DATE")
	@Temporal(TemporalType.DATE)
	private Date departureDate;
	@Column(name = "DEPARTURE_TIME")
	@Temporal(TemporalType.TIME)
	private Date departureTime;
	@Column(name = "ARRIVAL_DATE")
	@Temporal(TemporalType.DATE)
	private Date arrivalDate;
	@Column(name = "ARRIVAL_TIME")
	@Temporal(TemporalType.TIME)
	private Date arrivalTime;

	@OneToMany(mappedBy = "flightSchedule", cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Leg> legs = new ArrayList<Leg>();

	@OneToMany(mappedBy = "flightSchedule", cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Fare> fares = new ArrayList<Fare>();

	public FlightSchedule() {
		super();
	}

	public Long getId() {
		return id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departure) {
		this.departureAirport = departure;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrival) {
		this.arrivalAirport = arrival;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((arrivalAirport == null) ? 0 : arrivalAirport.hashCode());
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime
				* result
				+ ((departureAirport == null) ? 0 : departureAirport.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((departureTime == null) ? 0 : departureTime.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightSchedule other = (FlightSchedule) obj;
		if (arrivalAirport == null) {
			if (other.arrivalAirport != null)
				return false;
		} else if (!arrivalAirport.equals(other.arrivalAirport))
			return false;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (departureAirport == null) {
			if (other.departureAirport != null)
				return false;
		} else if (!departureAirport.equals(other.departureAirport))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (departureTime == null) {
			if (other.departureTime != null)
				return false;
		} else if (!departureTime.equals(other.departureTime))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		return true;
	}

	public List<Fare> getFares() {
		return fares;
	}

	@Override
	public String toString() {
		return "FlightSchedule [id=" + id + ", flightNumber=" + flightNumber
				+ ", departureAirport=" + departureAirport
				+ ", arrivalAirport=" + arrivalAirport + ", departureDate="
				+ departureDate + ", departureTime=" + departureTime
				+ ", arrivalDate=" + arrivalDate + ", arrivalTime="
				+ arrivalTime + "]";
	}

	public List<Leg> getLegs() {
		return legs;
	}

	public void addLeg(Leg l) {
		if (l != null) {
			l.setFlightSchedule(this);
			legs.add(l);
		}
	}

	public void addFare(Fare f) {
		if (f != null) {
			if (f.getId() != null) {
				for (Fare fa : fares) {
					if (fa.getId() == f.getId()) {
						fares.get(fares.indexOf(fa)).setBookings(
								f.getBookings());
						fares.get(fares.indexOf(fa)).setFareType(
								f.getFareType());
						fares.get(fares.indexOf(fa)).setOneWayFare(
								f.getOneWayFare());
						fares.get(fares.indexOf(fa)).setReturnFare(
								f.getReturnFare());
					}
				}
			} else {
				f.setFlightSchedule(this);
				fares.add(f);
			}

		}
	}
	
	public void removeFare(Fare fare) {
		if (fare != null) {
			fares.remove(fare);
			fare.setFlightSchedule(null);
		}
	}

	public List<Booking> getBookings() {
		List<Booking> bookings = new ArrayList<Booking>();
		for (Fare fare : fares) {
			bookings.addAll(fare.getBookings());
			break;
		}
		return bookings;

	}
}
