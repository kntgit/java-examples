package id.co.knt.airport.dto;

import id.co.knt.airport.entity.Passenger;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("passengerFilter")
public class PassengerFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -54963630350792564L;

	private String firstName;
	private String lastName;
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public boolean isQualify(Passenger passenger) {
		boolean result = true;
		if (firstName != null) {
			result = result && passenger.getFirstName().equalsIgnoreCase(firstName);
		}
		
		if (lastName != null) {
			result = result && passenger.getLastName().equalsIgnoreCase(lastName);
		}
		
		return result;
	}
}
