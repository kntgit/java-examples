package id.co.knt.airport.web.converter;

import id.co.knt.airport.entity.Airport;

import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("airportConverter")
@Stateless
public class AirportConverter implements Converter{
	@PersistenceContext
	EntityManager em;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 == null || arg2.equals(""))
			return null;
	    Long id = Long.valueOf(arg2);
	    Airport airport = em.find(Airport.class, id);
	    return airport;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (!(arg2 instanceof Airport))
			return "";
	    Airport airport = (Airport) arg2;
	    Long id = airport.getId();
	    return String.valueOf(id);
	}

}
