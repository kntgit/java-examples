package id.co.knt.airport.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Payment
 *
 */
@Entity
@Table(name="PAYMENTS")
	@NamedQueries({
		@NamedQuery(name = "Payment.findAll" , query="select p from Payment p")
	})
public class Payment implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Payment() {
		super();
	}
   
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="PAYMENT_DATE")
	@Temporal(TemporalType.DATE)
	private Date date;
	@Column(name="PAYMENT_NUMBER")
	private String paymentNumber;
	@Column(name="AMOUNT")
	private BigDecimal amount;
	@OneToMany(mappedBy = "payment", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Booking> bookings = new ArrayList<Booking>();

	@Override
	public String toString() {
		return "Payment [id=" + id + ", date=" + date + ", paymentNumber=" + paymentNumber + ", amount=" + amount
				+ ", bookings=" + bookings + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((bookings == null) ? 0 : bookings.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (bookings == null) {
			if (other.bookings != null)
				return false;
		} else if (!bookings.equals(other.bookings))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	
}
