package id.co.knt.airport.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LEGS")
public class Leg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1339100036603352889L;
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="ACTUAL_DEPARTURE_TIME")
	private Date actualDepartureTime;
	@Column(name="ACTUAL_ARRIVAL_TIME")
	private Date actualArrivalTime;
	@ManyToOne
	@JoinColumn(name="FLIGHT_SCHEDULE_ID")
	private FlightSchedule flightSchedule;
	@ManyToMany
	@JoinTable(name="JOURNEY_LEGS",
			joinColumns={@JoinColumn(name="LEG_ID", referencedColumnName="ID")},
			inverseJoinColumns={@JoinColumn(name="BOOKING_ID", referencedColumnName="ID")})
	private List<Booking> bookings = new ArrayList<Booking>();
	
	@Override
	public String toString() {
		return "Leg [id=" + id + ", actualDepartureTime=" + actualDepartureTime
				+ ", actualArrivalTime=" + actualArrivalTime + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((actualArrivalTime == null) ? 0 : actualArrivalTime
						.hashCode());
		result = prime
				* result
				+ ((actualDepartureTime == null) ? 0 : actualDepartureTime
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Leg other = (Leg) obj;
		if (actualArrivalTime == null) {
			if (other.actualArrivalTime != null)
				return false;
		} else if (!actualArrivalTime.equals(other.actualArrivalTime))
			return false;
		if (actualDepartureTime == null) {
			if (other.actualDepartureTime != null)
				return false;
		} else if (!actualDepartureTime.equals(other.actualDepartureTime))
			return false;
		return true;
	}

	public Date getActualDepartureTime() {
		return actualDepartureTime;
	}

	public void setActualDepartureTime(Date actualDepartureTime) {
		this.actualDepartureTime = actualDepartureTime;
	}

	public Date getActualArrivalTime() {
		return actualArrivalTime;
	}

	public void setActualArrivalTime(Date actualArrivalTime) {
		this.actualArrivalTime = actualArrivalTime;
	}

	public FlightSchedule getFlightSchedule() {
		return flightSchedule;
	}

	public void setFlightSchedule(FlightSchedule flightSchedule) {
		this.flightSchedule = flightSchedule;
	}

	public Long getId() {
		return id;
	}

	public List<Booking> getBookings() {
		return bookings;
	}
}
