package id.co.knt.airport.web;

import id.co.knt.airport.qualifier.FromDate;
import id.co.knt.airport.qualifier.ToDate;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

@SessionScoped
@Named
public class FromToDateFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3050641564954446424L;

	private Date from;
	private Date to;
	
	@Produces
	@FromDate
	public Date getFrom() {
		return from;
	}
	
	public void setFrom(Date from) {
		this.from = from;
	}
	
	@Produces
	@ToDate
	public Date getTo() {
		return to;
	}
	
	public void setTo(Date to) {
		this.to = to;
	}
	
}
