package id.co.knt.airport.web;

import id.co.knt.airport.datamodel.FlightFareDataModel;
import id.co.knt.airport.entity.Airport;
import id.co.knt.airport.entity.Booking;
import id.co.knt.airport.entity.Fare;
import id.co.knt.airport.entity.FareType;
import id.co.knt.airport.entity.FlightFare;
import id.co.knt.airport.entity.FlightSchedule;
import id.co.knt.airport.entity.Passenger;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("bookingBean")
@Stateful
@ConversationScoped
public class BookingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Inject
	private Conversation conversation;
	
	private static final Logger log = LoggerFactory.getLogger(BookingBean.class);

	private List<Airport> airports = new ArrayList<Airport>();
	private List<FareType> fareTypes = new ArrayList<FareType>();
	private List<FlightSchedule> flightSchedules = new ArrayList<FlightSchedule>();
	private Airport searchDeparture;
	private Airport searchArrival;
	private Date departureDate;
	private Date arrivalDate;
	private List<FlightFare> flightFares = new ArrayList<FlightFare>();
	private FlightFare selectedFlightFare;
	private FlightFare selectedFlightFare2;
	private Passenger selectedPassenger;
	private FlightFareDataModel flightFareDataModel;
	private List<Passenger> passengers = new ArrayList<Passenger>();
	private Boolean bookingType;
	private Integer passCount;
	private Integer counter;
	private Booking booking;

	@PostConstruct
	public void init() {
		
		airports = em.createNamedQuery("Airport.findAll", Airport.class)
				.getResultList();
		fareTypes = em.createNamedQuery("FareType.findAll", FareType.class)
				.getResultList();
		selectedPassenger = new Passenger();
		counter=1;

	}
	

	
	public String findFlight(){
		conversation.begin();
		flightFares.clear();
		if(bookingType){
			flightSchedules = em.createQuery("select fs from FlightSchedule fs where fs.departureAirport=:departure and fs.arrivalAirport=:arrival and fs.departureDate=:departureDate",FlightSchedule.class)
					.setParameter("departure", searchDeparture)
					.setParameter("arrival", searchArrival)
					.setParameter("departureDate", departureDate)
					.getResultList();
		}else{
			flightSchedules = em.createQuery("select fs from FlightSchedule fs where fs.departureAirport=:departure and fs.arrivalAirport=:arrival and fs.departureDate=:departureDate and fs.arrivalDate=:arrivalDate",FlightSchedule.class)
					.setParameter("departure", searchDeparture)
					.setParameter("arrival", searchArrival)
					.setParameter("departureDate", departureDate)
					.setParameter("arrivalDate", arrivalDate)
					.getResultList();
		}
		List<Fare> fares = new ArrayList<Fare>();
		for(FlightSchedule fs:flightSchedules){
			fares = em.createQuery("select f from Fare f where f.flightSchedule=:fs",Fare.class)
			.setParameter("fs", fs)
			.getResultList();
			for(Fare f :fares){
				flightFares.add(new FlightFare(getRandomModel(),fs, f));
			}
			
		}
		
		log.info("Flight Schedule : "+flightSchedules.size());
		flightFareDataModel = new FlightFareDataModel(flightFares);
		return "booking_page2";
	}
	
	public String findPassenger(){
		String result=null;
		List<Passenger> passengers2 = em.createNamedQuery("Passenger.findByFirstNameAndLastName",Passenger.class)
				.setParameter("firstName", selectedPassenger.getFirstName())
				.setParameter("lastName", selectedPassenger.getLastName())
				.getResultList();
		if(passengers2.size() > 0){
			selectedPassenger=passengers2.get(0);
		}else{
			em.persist(selectedPassenger);
			log.info("Saved id : "+selectedPassenger.getId());
		}
		passengers.add(selectedPassenger);
		selectedPassenger = new Passenger();
		if(counter < passCount){
			counter++;
			result=null;
		}else{
			booking = new Booking();
			booking.setCode(getCode());
			booking.setBookingType(bookingType);
			booking.setDate(new Date());
			booking.setFare(selectedFlightFare.getFare());
			for(Passenger p:passengers){
				p.getBookings().add(booking);
				booking.getPassengers().add(p);
			}
			booking.calculateTotal();
			result="booking_page4";
		}
		
		return result;
	}
	
	public String saveBooking(){
		em.persist(booking);
		conversation.end();
		return "booking_page5";
	}
	
	private String getRandomModel() {
		return UUID.randomUUID().toString().substring(0, 8);
	}

	private String getCode() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	
	public String passengerBooking(){
		if(selectedFlightFare==null && selectedFlightFare2 != null){
			selectedFlightFare=selectedFlightFare2;
		}
		log.info("Harga yang dipilih : "+selectedFlightFare.getFare().getOneWayFare());
		
		return "booking_page3";
	}

	public List<Airport> getAirports() {
		return airports;
	}

	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}

	public List<FareType> getFareTypes() {
		return fareTypes;
	}

	public void setFareTypes(List<FareType> fareTypes) {
		this.fareTypes = fareTypes;
	}

	public Airport getSearchDeparture() {
		return searchDeparture;
	}

	public void setSearchDeparture(Airport searchDeparture) {
		this.searchDeparture = searchDeparture;
	}

	public Airport getSearchArrival() {
		return searchArrival;
	}

	public void setSearchArrival(Airport searchArrival) {
		this.searchArrival = searchArrival;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public List<FlightSchedule> getFlightSchedules() {
		return flightSchedules;
	}

	public void setFlightSchedules(List<FlightSchedule> flightSchedules) {
		this.flightSchedules = flightSchedules;
	}

	public List<FlightFare> getFlightFares() {
		return flightFares;
	}

	public void setFlightFares(List<FlightFare> flightFares) {
		this.flightFares = flightFares;
	}

	public FlightFare getSelectedFlightFare() {
		return selectedFlightFare;
	}

	public void setSelectedFlightFare(FlightFare selectedFlightFare) {
		this.selectedFlightFare = selectedFlightFare;
	}



	public FlightFareDataModel getFlightFareDataModel() {
		return flightFareDataModel;
	}

	public void setFlightFareDataModel(FlightFareDataModel flightFareDataModel) {
		this.flightFareDataModel = flightFareDataModel;
	}

	public Passenger getSelectedPassenger() {
		return selectedPassenger;
	}

	public void setSelectedPassenger(Passenger selectedPassenger) {
		this.selectedPassenger = selectedPassenger;
	}
	public List<Passenger> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public Boolean getBookingType() {
		return bookingType;
	}

	public void setBookingType(Boolean bookingType) {
		this.bookingType = bookingType;
	}

	public Integer getPassCount() {
		return passCount;
	}

	public void setPassCount(Integer passCount) {
		this.passCount = passCount;
	}



	public Booking getBooking() {
		return booking;
	}



	public void setBooking(Booking booking) {
		this.booking = booking;
	}



	public FlightFare getSelectedFlightFare2() {
		return selectedFlightFare2;
	}



	public void setSelectedFlightFare2(FlightFare selectedFlightFare2) {
		this.selectedFlightFare2 = selectedFlightFare2;
	}



	public Integer getCounter() {
		return counter;
	}



	public void setCounter(Integer counter) {
		this.counter = counter;
	}




	
	
	
}
